/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.enolasoft.elpraxservices.vo;

import java.time.LocalDateTime;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;

/**
 *
 * @author xFighter911
 */
@XmlRootElement
@Data
public class EasyDateTime {

    private Integer day, month, year, hour, minute, second, nano;
    private Boolean isNull;

    public EasyDateTime() {
        isNull = true;
    }

}
