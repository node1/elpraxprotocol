/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.enolasoft.elpraxservices.vo;

import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Calendar;

/**
 *
 * @author xFighter911
 */
public class EasyDateUtil {

    public static EasyDate Ld2Ed(LocalDate ld) {

        EasyDate ed = new EasyDate();

        if (ld == null) {
            ed.setDay(1);
            ed.setMonth(1);
            ed.setYear(1900);
            ed.setIsNull(Boolean.TRUE);
        } else {
            ed.setDay(ld.getDayOfMonth());
            ed.setMonth(ld.getMonthValue());
            ed.setYear(ld.getYear());
            ed.setIsNull(Boolean.FALSE);
        }
        return ed;
    }

    public static LocalDate Ed2Ld(EasyDate ed) {

        if (ed.getIsNull()) {
            return null;
        }

        LocalDate ld = LocalDate.of(ed.getYear(), ed.getMonth(), ed.getDay());
        return ld;
    }

    public static EasyDateTime Ldt2Edt(LocalDateTime ldt) {
        EasyDateTime edt = new EasyDateTime();

        if (ldt == null) {
            edt.setIsNull(Boolean.TRUE);
            return edt;
        }

        edt.setYear(ldt.getYear());
        edt.setMonth(ldt.getMonthValue());
        edt.setDay(ldt.getDayOfMonth());
        edt.setHour(ldt.getHour());
        edt.setMinute(ldt.getMinute());
        edt.setSecond(ldt.getSecond());
        edt.setNano(ldt.getNano());

        edt.setIsNull(Boolean.FALSE);

        return edt;
    }

    public static EasyDate date2ed(Date date) {
        EasyDate ed = new EasyDate();

        if (date == null) {
            ed.setIsNull(Boolean.TRUE);
        } else {
            Calendar javaSux = Calendar.getInstance();
            javaSux.setTime(date);
            ed.setDay(javaSux.get(Calendar.DAY_OF_MONTH));
            ed.setMonth(javaSux.get(Calendar.MONTH));
            ed.setYear(javaSux.get(Calendar.YEAR));
            ed.setIsNull(Boolean.FALSE);
        }

        return ed;
    }

    public static LocalDateTime Edt2Ldt(EasyDateTime edt) {
        if (edt.getIsNull()) {
            return null;
        }

        LocalDateTime ldt = LocalDateTime.of(edt.getYear(), edt.getMonth(), edt.getDay(), edt.getHour(), edt.getMinute(), edt.getSecond(), edt.getNano());
        return ldt;
    }

}
