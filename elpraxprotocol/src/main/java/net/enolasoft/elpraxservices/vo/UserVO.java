/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.enolasoft.elpraxservices.vo;

import java.time.LocalDate;
import java.time.LocalDateTime;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;

/**
 *
 * @author xFighter911
 */
@Data
@XmlRootElement
public class UserVO {

    private int usno;
    private String name, pass, email, firstName, lastName;
    private EasyDate DOB;
    private EasyDateTime validUntil;
    private boolean activated;

    private int registrationNo;  // PK for the business

    private String role;
    private String request; // request code if there is any user-related request still open   

}
