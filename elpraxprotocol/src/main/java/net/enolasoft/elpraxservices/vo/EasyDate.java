/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.enolasoft.elpraxservices.vo;

import java.time.LocalDate;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;

/**
 *
 * @author xFighter911
 */
@XmlRootElement
@Data
public class EasyDate {

    private Integer day, month, year;
    private Boolean isNull = false;

    public EasyDate() {
        isNull = true;
    }

}
