/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.enolasoft.elpraxservices.vo;

import java.time.LocalDate;
import java.time.LocalDateTime;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;

/**
 *
 * @author xFighter911
 */
@Data
@XmlRootElement
public class PatientVO {

    private long number;

    private String salutation;
    private String title;
    private String firstName;
    private String lastName;
    private String maidenName;
    private LocalDate DOB;

    private String address1;
    private String address2;

    private String zipcode;
    private String city;
    private String state;
    private String countryCode;

    private String phone1, phone2, mobile;
    private String email;

    private LocalDateTime created;
    private LocalDateTime modified;

    private String notes;

}
