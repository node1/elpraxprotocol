/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.enolasoft.elpraxservices.protocol.auth;

import java.time.LocalDateTime;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;
import net.enolasoft.elpraxservices.vo.EasyDateTime;
import net.enolasoft.elpraxservices.vo.EasyDateUtil;
import net.enolasoft.elpraxservices.vo.UserVO;

/**
 *
 * @author steve
 */
@Data
@XmlRootElement
public class AuthenticationResponse {

    private Status status;
    private String token;
    private EasyDateTime validUntil;
    private UserVO user;

    public enum Status {
        SUCCESS, FAILED, NOSUCHUSER, NOTACTIVATED
    };

    public AuthenticationResponse() {
    }

    public AuthenticationResponse(Status status, String token, UserVO user, LocalDateTime validUntil) {
        this.status = status;
        this.token = token;
        this.validUntil = EasyDateUtil.Ldt2Edt(validUntil);
        this.user = user;
    }

    
}
