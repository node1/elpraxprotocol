/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.enolasoft.elpraxservices.protocol;

import lombok.Data;

/**
 *
 * @author xFighter911
 */
@Data
public class QueryPatientRequest {

    String token;

    private String query;
}
