/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.enolasoft.elpraxservices.protocol.auth;

import lombok.Data;

/**
 *
 * @author xFighter911
 */
@Data
public class RegistrationResponse {

    public enum Status {
        SUCCESS, FAILED, USEREXISTS, INVALIDDATA, EMAIL_REGISTERED
    };
    private Status status;
    private String technichalMessage; // technical caus' its not intended for user-display

    public RegistrationResponse(Status status) {
        this.status = status;
    }

    public RegistrationResponse(Status status, String tech) {
        this.status = status;
        this.technichalMessage = tech;
    }

}
