/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.enolasoft.elpraxservices.protocol.auth;

import lombok.Data;

/**
 *
 * @author xFighter911
 */
@Data
public class RecoverPasswordResponse {

    private final Status status;

    public enum Status {
        SUCCESS, FAILED, INVALIDEMAIL
    };

    public RecoverPasswordResponse(Status status) {
        this.status = status;
    }
}
