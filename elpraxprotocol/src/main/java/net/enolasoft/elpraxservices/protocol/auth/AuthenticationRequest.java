/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.enolasoft.elpraxservices.protocol.auth;

import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;

/**
 *
 * @author steve
 */
@Data
@XmlRootElement
public class AuthenticationRequest {

    private String userName, password;

    public AuthenticationRequest() {
    }

    public AuthenticationRequest(String user, String pass) {
        this.userName = user;
        this.password = pass;
    }
}
