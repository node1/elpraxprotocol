/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.enolasoft.elpraxservices.protocol.auth;

import lombok.Data;
import net.enolasoft.elpraxservices.vo.EasyDate;

/**
 *
 * @author xFighter911
 */
@Data
public class RegistrationRequest {

    private String businessName;
    private String lastName, firstName;
    private String address1, address2, zipc, city, countryCode;
    private String email, username, password;
    private EasyDate DOB;
    private boolean acceptDP, acceptTC;

}
